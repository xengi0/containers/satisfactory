# satisfactory

Dedicated Server for Satisfactory

## Volumes

- `/gamefiles`: holds the gameserver files and config and will be installed/updated on every start
- `/savegames`: persists the savegames and server settings

## Run it

Assuming you want to persist game files and saves at `/opt/satisfactory/`:

```
podman run \
    -d \
    -u 900 \
    -p 15777:15777/udp \
    -p 15000:15000/udp \
    -p 7777:7777/udp \
    -v /opt/satisfactory/gamefiles:/gamefiles \
    -v /opt/satisfactory/savegames:/savegames \
    registry.gitlab.com/xengi-containers/satisfactory:latest
```

## Configuration

Config files can be found inside the container under `/gamefiles/Engine/Saved/Config/LinuxServer`.

---

Made with ❤️ and 🍺.
