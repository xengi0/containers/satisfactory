#!/bin/sh

/usr/games/steamcmd \
    +force_install_dir /gamefiles \
    +login anonymous \
    +app_update 1690800 validate \
    +quit

# fix: [S_API] SteamAPI_Init(): Sys_LoadModule failed to load: /home/steam/.steam/sdk64/steamclient.so
mkdir -p /home/steam/.steam/sdk64
ln -sf /home/steam/.local/share/Steam/steamcmd/linux64/steamclient.so /home/steam/.steam/sdk64/steamclient.so

/gamefiles/FactoryServer.sh -log -unattended
